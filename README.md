# Office Zombies

## Summary
This is a text adventure that I've created to learn Rust. If you've somehow stumbled on to this repository, this text adventure is set in an office setting, and it mainly asks for yes or no answers from the user. This is because I'm learning Rust and I'm still getting a hang of things. I'm a big believer in incremental learning.

If you're interested in learning from this code, or making your own version, please do so; I believe in free education, and if this can help you in anyway, then by all means.
